(function ($) {
  'use strict';

  /**
   * By default, the calendar icon is not shown unless you don't focus on the
   * input field. Here we're triggering the change event to stimulate it.
   *
   * @type {{attach: Drupal.behaviors.datepickericon.attach}}
   */
  Drupal.behaviors.datepickericon = {
    attach: function () {
      for (var id in Drupal.settings.datePopup) {
        $('#' + id).once('dateicon', function () {
          var datePopup = Drupal.settings.datePopup[id];
          switch (datePopup.func) {
            case 'dateicon':
              $(this).datepicker(datePopup.settings).trigger('change');
              break;

            case 'dateicon-mask':
              $(this).datepicker(datePopup.settings).inputmask(datePopup.settings.maskFormat).trigger('change');
              break;
          }
        });
      }
    }
  }

})(jQuery);
