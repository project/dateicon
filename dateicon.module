<?php

/**
 * Implements hook_element_info().
 */
function dateicon_element_info() {
  $date_types = date_popup_element_info();
  $types['dateicon'] = $date_types['date_popup'];
  // Additional default parameters.
  $types['dateicon']['#icon'] = base_path() . drupal_get_path('module', 'date_api') . '/images/calendar.png';
  $types['dateicon']['#func'] = 'dateicon';
  $types['dateicon']['#inputmask'] = FALSE;
  $types['dateicon']['#inputmask_format'] = 'dd/mm/yyyy';

  // Process callback.
  $types['dateicon']['#process'] = array('dateicon_element_process');
  return $types;
}

/**
 * Dateicon element process callback.
 */
function dateicon_element_process($element, &$form_state, $form) {
  $element = date_popup_element_process($element, $form_state, $form);

  $element['date']['#attached']['js'] = array(
    drupal_get_path('module', 'dateicon') . '/dateicon.js',
  );

  // Either the mask should be added.
  $add_mask = dateicon_should_add_mask($element);

  // If the inputmask library is installed, add a mask to the date field.
  if ($add_mask) {
    // Pass the date format.
    $element['date']['#attached']['libraries_load'][] = array('inputmask');
  }

  $css_id = $element['date']['#id'];
  $js = drupal_add_js();
  foreach ($js['settings']['data'] as $i => $data) {
    if (isset($data['datePopup'][$css_id])) {
      // Remove unneeded parameters.
      foreach (array('autoPopUp') as $property) {
        unset($data['datePopup'][$css_id]['settings'][$property]);
      }

      // Add necessary parameters for the button.
      $data['datePopup'][$css_id]['settings']['showOn'] = 'button';
      $data['datePopup'][$css_id]['settings']['buttonImage'] = $element['#icon'];
      $data['datePopup'][$css_id]['settings']['buttonImageOnly'] = TRUE;
      $data['datePopup'][$css_id]['settings']['buttonText'] = t('Select date');

      // Add required parameters.
      if ($add_mask) {
        $data['datePopup'][$css_id]['func'] = 'dateicon-mask';
        $data['datePopup'][$css_id]['settings']['maskFormat'] = $element['#inputmask_format'];
      }
      else {
        $data['datePopup'][$css_id]['func'] = 'dateicon';
      }

      drupal_add_js($data, 'setting');
      break;
    }
  }
  return $element;
}

/**
 * Implements hook_libraries_info().
 *
 * Define inputmask library.
 */
function dateicon_libraries_info() {
  $libraries['inputmask'] = array(
    'name' => 'Inputmask',
    'vendor url' => 'http://robinherbots.github.io/Inputmask/',
    'download url' => 'https://github.com/RobinHerbots/Inputmask/releases',
    'version arguments' => array(
      'file' => 'component.json',
      'pattern' => '@"version": "([0-9\.a-z\-]+)"@',
      'lines' => 20,
      'cols' => 400,
    ),
    'files' => array(
      'js' => array(
        'dist/min/inputmask/inputmask.min.js',
        'dist/min/inputmask/inputmask.date.extensions.min.js',
        'dist/min/jquery.inputmask.bundle.min.js',
      ),
    ),
    'variants' => array(
      'source' => array(
        'files' => array(
          'js' => array(
            'dist/inputmask/inputmask.js',
            'dist/inputmask/inputmask.date.extensions.js',
            'dist/jquery.inputmask.bundle.js',
          ),
        ),
      ),
      'minified' => array(
        'files' => array(
          'dist/min/inputmask/inputmask.min.js',
          'dist/min/inputmask/inputmask.date.extensions.min.js',
          'dist/min/jquery.inputmask.bundle.min.js',
        ),
      ),
    ),
  );

  return $libraries;
}

/**
 * Should we/can we add the mask support.
 *
 * @param array $element
 * @return bool
 */
function dateicon_should_add_mask($element) {
  if (!module_exists('libraries')) {
    return FALSE;
  }

  return ($element['#inputmask'] && ($library = libraries_detect('inputmask')) && !empty($library['installed']));
}
