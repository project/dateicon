This module extends the date_popup module to provide a JQuery UI icon
datepicker. For an example of this see:
https://jqueryui.com/datepicker/#icon-trigger

In order to apply the new FAPI type you'll have to alter your existing
date_popup elements #type to #dateicon